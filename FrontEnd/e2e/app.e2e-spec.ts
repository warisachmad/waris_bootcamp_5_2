import { TemplateDefaultPage } from './app.po';

describe('template-default App', () => {
  let page: TemplateDefaultPage;

  beforeEach(() => {
    page = new TemplateDefaultPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
