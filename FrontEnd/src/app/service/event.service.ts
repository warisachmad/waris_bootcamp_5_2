import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class EventService {

  constructor(private http:Http) { }

  // Get Event List
  getEventList(){

    let token = localStorage.getItem("token");
    let headers = new Headers({ "Authorization" : "Bearer " + token });
    let options = new RequestOptions({ headers : headers });

    console.log(token);

    return this.http.get('http://localhost:8000/api', options)
    .map(result => result.json());
  }

  // Add Event
  addEvent(nama_event:string, waktu_event:string, lokasi_event:string){

    let token = localStorage.getItem("token");
    let data = {
      "nama_event":nama_event,
      "waktu_event":waktu_event,
      "lokasi_event":lokasi_event
    }
    let body = JSON.stringify(data);
    let headers = new Headers({
      "Content-Type" : "application/json",
      "Authorization" : "Bearer " + token
    });
    let options = new RequestOptions({ headers : headers });

    return this.http.post('http://localhost:8000/api/save', body, options)
    .map(result => result.json());

  }

}
