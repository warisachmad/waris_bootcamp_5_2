import { Component, OnInit, Input } from '@angular/core';

import { EventService } from '../service/event.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  @Input('acara') event: Object[];
  @Input('pemesanan') order: Object[];

  constructor(private api:EventService) { }

  // event:object[];

  ngOnInit() {
    console.log(event);
  }

  // Get data
  GetData(){
    this.api.getEventList()
            .subscribe(result => this.event = result);
  }

  Buy(tiket_id){
    for (var i = 0; i < this.order.length; i++) {
      if (this.order[i]["tiket_id"] == tiket_id) {
        this.order[i]["jumlah_pesan"]++;
        break;
      }
    }
  }

}
