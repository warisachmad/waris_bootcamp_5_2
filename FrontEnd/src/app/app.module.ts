import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventMasterComponent } from './event-master/event-master.component';

import { HttpModule } from '@angular/http';
import { EventService } from './service/event.service';

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventMasterComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpModule
  ],
  providers: [EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
