import { Component, OnInit, Input } from '@angular/core';

import { EventService } from '../service/event.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  @Input('acara') event: Object[];

  constructor(private api:EventService) { }

  newNamaEvent : string = "";
  newWaktuEvent : string = "";
  newLokasiEvent : string = "";

  // event:object[];

  ngOnInit() {
  }

  // Add Data
  addData(){
    this.api.addEvent( this.newNamaEvent, this.newWaktuEvent, this.newLokasiEvent )

    .subscribe(result => this.event = result);

    this.newNamaEvent = "";
    this.newWaktuEvent = "";
    this.newLokasiEvent = "";

  }

}
