<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\acara;

class eventController extends Controller
{
    function getEvent(){

        $event = acara::get();
        return response()->json($event, 200);
    }

    function saveEvent(Request $request){
        DB::beginTransaction();

        try{
             $this->validate($request, [
                'nama_acara' => 'required',
                'waktu_acara' => 'required',
                'lokasi_acara' => 'required' 
              ]);

            $nama_acara = $request->input('nama_acara');
            $waktu_acara = $request->input('waktu_acara');
            $lokasi_acara = $request->input('lokasi_acara');

            $event = new acara;
            $event->nama_acara = $nama_acara;
            $event->waktu_acara = $waktu_acara;
            $event->lokasi_acara = $lokasi_acara;
            $event->save();

            $event = acara::get();

            DB::commit();

            return response()->json($event, 200);
        }
        catch(\Exception $e){
            DB::rollback();

            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

}
